﻿using DAL.Entities;
using DAL.ModelsDTO.ConsoleClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Interfaces
{
    public abstract class BaseService
    {
        public abstract List<Teams> teams { get; set; }
        public abstract List<Users> users { get; set; }
        public abstract List<Tasks> tasks { get; set; }
        public abstract List<Projects> projects { get; set; }
        public abstract void LoadData();

        public abstract void MergeData();
         
        /// <summary>
        /// 1. Отримати кількість тасків у проекті конкретного користувача
        /// </summary>
        /// <param name="userId">Id користувача</param>
        /// <returns>словник, де key проект, а value кількість тасків</returns>        
        public Dictionary<Projects, int> GetUserTasksCountInProjects(int userId)
        {
            CheckInput(userId);
            return projects.Where(x => x.Tasks.Any(y => y.PerformerId == userId))
                           .ToDictionary(proj => proj, x => x.Tasks.Where(x => x.PerformerId == userId).Count());
        }
        protected void CheckInput(int id)
        {
            if (id <= 0)
                throw new ArgumentException("id");
            CheckInput();
        }
        protected void CheckInput()
        {
            if (projects == null)
                throw new InvalidOperationException("Data not loaded");
        }

        /// <summary>
        /// 2. Отримати список тасків, призначених для конкретного користувача, де name таска <45 символів
        /// </summary>
        /// <param name="userId">Id користувача</param>
        /// <returns>колекція з тасків</returns>
        public List<Tasks> GetUserTaskWithCondition(int userId)
        {
            CheckInput(userId);
            return projects.SelectMany(x => x.Tasks).Where(x => (x.PerformerId == userId) && x.Name.Length < 45).ToList();
        }

        /// <summary>
        /// 3. Отримати список з колекції тасків, які виконані в поточному (2021) році
        /// </summary>
        /// <param name="userId">Id користувача</param>
        /// <returns>список (id, name) з колекції тасків</returns>
        public List<UserTasksFinishedDTO> GetUserTaskFinishedThisYear(int userId)
        {
            CheckInput(userId);
            return projects.SelectMany(x => x.Tasks)
                .Where(x => x.PerformerId == userId && x.FinishedAt?.Year == 2021)
                .Select(x => new UserTasksFinishedDTO
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToList();
        }

        /// <summary>
        /// 4. Отримати список з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації
        /// користувача за спаданням, а також згрупованих по командах.
        /// </summary> 
        /// <returns>List<TeamsSortedDTO></returns>
        public List<TeamsSortedDTO> GetTeamsWithSortedUserOlderTen()
        {
            CheckInput();
            return projects.Select(x => x.Team)
                             .Where(x => x.Users.Where(x => x.BirthDay.Year < 2011).Count() == x.Users.Count())
                             .GroupBy(x => x.Id)
                             .SelectMany(x => x)
                             .Select(team => new TeamsSortedDTO
                             {
                                 Id = team.Id,
                                 Name = team.Name,
                                 Users = team.Users.OrderByDescending(x => x.RegisteredAt).ToList()
                             }).Distinct().ToList();
        }

        /// <summary>
        /// 5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням). 
        /// </summary>
        /// <returns>List<UsersWithTasksDTO></returns>
        public List<UsersWithTasksDTO> GetUserListWithTasks()
        {
            CheckInput();
            return projects.SelectMany(x => x.Tasks)
                            .Select(x => x.Performer)
                            .OrderBy(x => x.FirstName)
                            .Distinct()
                            .Select(usr => new UsersWithTasksDTO
                            {
                                Id = usr.Id,
                                TeamId = usr.TeamId,
                                FirstName = usr.FirstName,
                                LastName = usr.LastName,
                                Email = usr.Email,
                                RegisteredAt = usr.RegisteredAt,
                                BirthDay = usr.BirthDay,
                                Tasks = projects.Where(x => x.Tasks.Where(x => x.PerformerId == usr.Id).Count() > 0)
                                                .SelectMany(x => x.Tasks.Where(x => x.PerformerId == usr.Id))
                                                .OrderByDescending(x => x.Name).ToList()
                            }).ToList();
        }

        /// <summary>
        /// 6. Отримати статистику користувача
        /// </summary>
        /// <param name="userId">Id користувача</param>
        /// <returns>UserStatisticsDTO </returns>
        public UserStatisticsDTO GetUserStatistics(int userId)
        {
            CheckInput(userId);
            return projects.Where(x => x.Tasks.Where(x => x.PerformerId == userId).Count() > 0)
                            .OrderByDescending(x => x.CreatedAt)
                            .Select(proj => new UserStatisticsDTO
                            {
                                User = projects.SelectMany(x => x.Tasks).Where(x => x.PerformerId == userId).Select(x => x.Performer).FirstOrDefault(),
                                LastProject = proj,
                                TasksInLastProject = proj.Tasks.Count(),
                                CanceledTasksCount = projects.Where(x => x.Tasks.Where(x => x.PerformerId == userId).Count() > 0)
                                                             .SelectMany(x => x.Tasks.Where(x => x.PerformerId == userId))
                                                             .Where(x => !x.FinishedAt.HasValue)
                                                             .Count(),
                                LongestTask = projects.Where(x => x.Tasks.Where(x => x.PerformerId == userId).Count() > 0)
                                                             .SelectMany(x => x.Tasks.Where(x => x.PerformerId == userId))
                                                             .OrderByDescending(x => x.FinishedAt.HasValue ?
                                                                                     x.FinishedAt.Value.Subtract(x.CreatedAt) :
                                                                                     DateTime.Now.Subtract(x.CreatedAt))
                                                             .FirstOrDefault()
                            }).FirstOrDefault();
        }

        /// <summary>
        /// 7. Отримати статистику проекту
        /// </summary>
        /// <returns>List<ProjectStatisticsDTO></returns>
        public List<ProjectStatisticsDTO> GetProjectStatistics()
        {
            CheckInput();
            return projects.Select(proj => new ProjectStatisticsDTO
            {
                Project = proj,
                LongestDescriptionTask = proj.Tasks.OrderByDescending(x => x.Description.Length).FirstOrDefault(),
                ShortestNameTask = proj.Tasks.OrderBy(x => x.Name.Length).FirstOrDefault(),
                UsersCntWithCondition = ((proj.Description.Length > 20) || (proj.Tasks.Count() > 3)) ? proj.Team.Users.Count() : null
            }).ToList();
        }


        public virtual void AddUser(Users user)
        {
            users.Add(user);
            MergeData();
        }
        public virtual void MarkTaskAsFinished(int taskId)
        {
            tasks.Where(x=>x.Id == taskId).FirstOrDefault().FinishedAt = DateTime.Now; 
            MergeData();
        }
        public virtual void AddUserToTeam(int userId,int teamId) 
        {
            users.Where(x=>x.Id == userId).FirstOrDefault().TeamId = teamId; 
            MergeData();
        }

    }
}