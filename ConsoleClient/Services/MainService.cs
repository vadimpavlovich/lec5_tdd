﻿using System;
using System.Collections.Generic;
using System.Linq; 
using BLL.Interfaces;
using DAL.Entities;
using DAL.ModelsDTO.ConsoleClient;

namespace ConsoleClient.Services
{
    public class MainService : BaseService
    {
        public RequestService api { get; set; }
        public override List<Projects> projects { get; set; }
        public override List<Teams> teams { get; set; }
        public override List<Users> users { get; set; }
        public override List<Tasks> tasks { get; set; }

        public MainService()
        {
            api = new RequestService();
        }  
        public override void LoadData() 
        {
            api.LoadData();
            projects = api.MergeData();
        }
        public override void MergeData()
        {
            api.LoadData();
            projects = api.MergeData();
        }


    }
}
