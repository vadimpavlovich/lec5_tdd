using BLL.Interfaces;
using System;
using System.Linq;
using Xunit;

namespace BLL.Tests
{
    public class BaseServiceTests : IDisposable
    {
        readonly FakeBaseService baseService;

        public BaseServiceTests()
        {
            baseService = new FakeBaseService();
        }

        public void Dispose()
        {
            baseService?.Dispose();
        }

        [Fact]
        public void Method1_GetUserTasksCountInProjects_WhenOneProjectWithOneUserWithOneTeam_ThenSingle()
        {
            baseService.LoadFakeData(1);

            var dict = baseService.GetUserTasksCountInProjects(1);

            Assert.Single(dict);
        }

        [Fact]
        public void Method1_GetUserTasksCountInProjects_WhenOneProjectWithincorrectId_ThenThrowError()
        {
            baseService.LoadFakeData(1);
             
            Assert.Throws<ArgumentException>(() => baseService.GetUserTasksCountInProjects(-1)); 
        }

        [Fact]
        public void Method2_GetUserTaskWithCondition_WhenTaskNameLess45_ThenSingle()
        {
            baseService.LoadFakeData(1);
            baseService.db.Tasks.FirstOrDefault().Name = baseService.seed.gen.Random.String(10);
            baseService.db.SaveChanges(); 

            var tasks = baseService.GetUserTaskWithCondition(1);

            Assert.Single(tasks);
            Assert.True(tasks[0].Name.Length < 45, "Expected Name to be less than 45.");
        }

        [Fact]
        public void Method2_GetUserTaskWithCondition_WhenIncorrectId_ThenThrowException()
        {
            baseService.LoadFakeData(1);
            baseService.db.Tasks.FirstOrDefault().Name = baseService.seed.gen.Random.String(10);
            baseService.db.SaveChanges();
             
            Assert.Throws<ArgumentException>(() => baseService.GetUserTaskWithCondition(-1));
        }
        [Fact]
        public void Method2_GetUserTaskWithCondition_WhenTaskNameMore45_ThenEmpty()
        {
            baseService.LoadFakeData(1);
            baseService.db.Tasks.FirstOrDefault().Name = baseService.seed.gen.Random.String(46);
            baseService.db.SaveChanges();

            var tasks = baseService.GetUserTaskWithCondition(1);

            Assert.Empty(tasks);
        }

        [Fact]
        public void Method3_GetUserTaskFinishedThisYear_When2tasksMoreAndEqual_ThenSingle()
        {
            baseService.LoadFakeData(1);
            var task = baseService.db.Tasks.FirstOrDefault();
            task.FinishedAt = new DateTime(2021, 1, 1);
            baseService.db.SaveChanges();
            var AnotherTask = task;
            AnotherTask.FinishedAt = new DateTime(2020, 1, 1);
            AnotherTask.Id = 2;
            baseService.db.Tasks.Add(AnotherTask);
            baseService.db.SaveChanges();
            baseService.MergeData();

            var tasks = baseService.GetUserTaskFinishedThisYear(1);

            Assert.Single(tasks);
        }

        [Fact]
        public void Method4_GetTeamsWithSortedUserOlderTen_When2usersOlderTen_ThenSingleAndSorted()
        {
            baseService.LoadFakeData(1);
            baseService.AddAnotherUserOlderTenAndPrevReg();

            var team = baseService.GetTeamsWithSortedUserOlderTen(); 

            Assert.Single(team);
            Assert.True(team[0].Users.Count == 2, "Not enough users");
            Assert.True(team[0].Users[1].Id == 2,"Users not sorted"); 
        }

        [Fact]
        public void Method5_GetUserListWithTasks_When2usersWithDifferentNames_ThenTwoSorted()
        {
            baseService.LoadFakeData(1);
            baseService.AddAnotherUserWithNewTaskAndRename(); 

            var res = baseService.GetUserListWithTasks();
             
            Assert.True(res.Count == 2, "Not enough users");
            Assert.True(res[0].Id == 2, "Users not sorted");
        }

        [Fact]
        public void Method6_GetUserStatistics_WithOneCanceledTasksCount_ThenOneTaskCount()
        {
            baseService.LoadFakeData(1);
            var task = baseService.db.Tasks.First();
            task.FinishedAt = null;
            baseService.db.SaveChanges();
            baseService.MergeData(); 

            var res = baseService.GetUserStatistics(1);

            Assert.NotNull(res); 
            Assert.True(res.CanceledTasksCount == 1, "May be one notfinished task");
        }

        [Fact]
        public void Method6_GetUserStatistics_WithOneCanceledTasksCountWithInvalidId_ThenThrow()
        {
            baseService.LoadFakeData(1);
            var task = baseService.db.Tasks.First();
            task.FinishedAt = null;
            baseService.db.SaveChanges();
            baseService.MergeData();
             
            Assert.Throws<ArgumentException>(() => baseService.GetUserStatistics(-1)); 
        }

        [Fact]
        public void Method7_GetProjectStatistics_WithOneTasks_ThenSameLongestAndShortestTask()
        {
            baseService.LoadFakeData(1); 

            var res = baseService.GetProjectStatistics();

            Assert.Single(res);
            Assert.Same(res.First().LongestDescriptionTask, res.First().ShortestNameTask);
        }

        [Fact]
        public void AddUser_ThenUserAdded()
        {
            baseService.LoadFakeData(1);
            var user = baseService.seed.GetUsersList(2).FirstOrDefault();
            baseService.AddUser(user); 

            Assert.True(baseService.users.Count==2); 
        }

        [Fact]
        public void AddNUllUser_ThenThrowException()
        {
            baseService.LoadFakeData(1); 
           
            Assert.Throws<ArgumentNullException>(() => baseService.AddUser(null)); 
        }

        [Fact]
        public void MarkTaskAsFinished_ThenMarked()
        {
            baseService.LoadFakeData(1); 
            baseService.MarkTaskAsFinished(1);

            Assert.NotNull(baseService.tasks.First().FinishedAt);
        }


        [Fact]
        public void AddUserToTeam_ThenAdded()
        {
            baseService.LoadFakeData(1);
            var user = baseService.seed.GetUsersList(2).FirstOrDefault();
            user.TeamId = 2;
            baseService.AddUser(user);

            baseService.AddUserToTeam(2,1);
             
            Assert.True(baseService.teams.First().Users.Count() == 2);
        }
    }
}