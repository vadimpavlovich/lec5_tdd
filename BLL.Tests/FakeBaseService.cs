﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Interfaces;
using DAL;
using DAL.Entities;
using DAL.Services;
using Microsoft.EntityFrameworkCore;

namespace BLL.Tests
{
    public class FakeBaseService : BaseService, IDisposable
    {
        public override List<Teams> teams { get; set; }
        public override List<Users> users { get; set; }
        public override List<Tasks> tasks { get; set; }
        public override List<Projects> projects { get; set; }

        public Context db;
        public SeederService seed;

        public FakeBaseService()
        {
            var options = new DbContextOptionsBuilder<Context>()
                      .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                      .Options;
            db = new Context(options);

        }
        public void LoadFakeData(int recordsCount = 0, int Id = 1)
        {
            seed = new SeederService(recordsCount);
            seed.FillDataToDb(db, Id);
            MergeData();
        }

        public override void MergeData()
        {
            users= db.Users.ToList();
            tasks = db.Tasks.ToList();
            teams = db.Teams.ToList(); 
            projects = db.Projects.Include(x => x.Author)
                                        .Include(x => x.Team)
                                        .ThenInclude(x => x.Users)
                                        .Include(x => x.Tasks)
                                        .ThenInclude(x => x.Performer)
                                        .ToList();
        }
        public override void AddUser(Users user)
        {
            db.Users.Add(user);
            db.SaveChanges();
            MergeData();
        } 
        public override void MarkTaskAsFinished(int taskId)
        { 
            db.Tasks.Where(x => x.Id == taskId).FirstOrDefault().FinishedAt = DateTime.Now;
            db.SaveChanges(); 
            MergeData();
        }
        public override void AddUserToTeam(int userId, int teamId)
        {
            db.Users.Where(x => x.Id == userId).FirstOrDefault().TeamId = teamId;
            db.SaveChanges();
            MergeData();
        }
        public override void LoadData()
        {

        }

        public void Dispose()
        {
            db?.Dispose();
        }

        public void AddAnotherUserWithNewTaskAndRename()
        {
            var user = db.Users.FirstOrDefault();
            user.FirstName = "b";
            db.SaveChanges();

            var newTask = seed.GetTasksList(2).FirstOrDefault();
            newTask.PerformerId = 2; 
            db.Tasks.Add(newTask);
            db.SaveChanges();

            var AnotherUser = seed.GetUsersList(2).FirstOrDefault();
            AnotherUser.FirstName = "a"; 
            db.Users.Add(AnotherUser);
            db.SaveChanges();
            MergeData();
        }

        public void AddAnotherUserOlderTenAndPrevReg()
        {
            var user = db.Users.FirstOrDefault();
            user.BirthDay = new DateTime(2000, 1, 1);
            db.SaveChanges();
            var AnotherUser = user;
            AnotherUser.BirthDay = new DateTime(2000, 1, 1);
            AnotherUser.Id = 2;
            AnotherUser.RegisteredAt = user.RegisteredAt.AddDays(-1);
            db.Users.Add(AnotherUser);
            db.SaveChanges();
            MergeData();
        }
    }
}
