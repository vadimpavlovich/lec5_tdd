﻿using AutoMapper;
using DAL.Entities;
using DAL.ModelsDTO.WebAPI;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Microsoft.EntityFrameworkCore;
using DAL.Validators;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly Context db;
        private readonly IMapper mapper;
        public TasksController(Context db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<TaskDTO>>> GetTasksList()
        {
            List<Tasks> Tasks = await db.Tasks.ToListAsync();
            return Ok(mapper.Map<List<TaskDTO>>(Tasks));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetTaskByID(int id)
        {
            if (!await db.Tasks.Where(x => x.Id == id).AnyAsync())
                return NotFound();
            Tasks Task = db.Tasks.Where(x => x.Id == id).FirstOrDefault();
            return Ok(mapper.Map<TaskDTO>(Task));
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> CreateTask([FromBody] CreateTaskDTO Task)
        {
            var validator = new TasksValidator();
            Tasks newTask = mapper.Map<Tasks>(Task);
            if (!validator.Validate(newTask).IsValid)
                return BadRequest(validator.Validate(newTask).Errors);
            db.Tasks.Add(newTask);
            await db.SaveChangesAsync();
            return Created("", mapper.Map<TaskDTO>(newTask));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TaskDTO>> UpdateTask(int id, [FromBody] UpdateTaskDTO Task)
        {
            var validator = new TasksValidator();
            Tasks newTask = mapper.Map<Tasks>(Task);
            if (!validator.Validate(newTask).IsValid)
                return BadRequest(validator.Validate(newTask).Errors);
            var updated = mapper.Map<Tasks>(Task); 
            var exist = await db.Tasks.FirstOrDefaultAsync(x => x.Id == id);
            if (exist != null)
            {
                updated.Id = exist.Id;
                db.Tasks.Remove(exist);
                db.Tasks.Add(updated);
                await db.SaveChangesAsync();
            } 
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TaskDTO>> DeleteTask(int id)
        {
            var Task = await db.Tasks.FirstOrDefaultAsync(x => x.Id == id);
            if (Task != null)
            {
                db.Tasks.Remove(Task);
                await db.SaveChangesAsync();
            }
            return NoContent();
        }
    }
}
