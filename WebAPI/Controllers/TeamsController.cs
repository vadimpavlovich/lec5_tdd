﻿using AutoMapper;
using DAL.Entities;
using DAL.ModelsDTO.WebAPI;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Microsoft.EntityFrameworkCore;
using DAL.Validators;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly Context db;
        private readonly IMapper mapper;
        public TeamsController(Context db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<TeamDTO>>> GetTeamsList()
        {
            List<Teams> Teams = await db.Teams.ToListAsync();
            return Ok(mapper.Map<List<TeamDTO>>(Teams));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetTeamByID(int id)
        {
            if (!await db.Teams.Where(x => x.Id == id).AnyAsync())
                return NotFound();
            Teams Team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
            return Ok(mapper.Map<TeamDTO>(Team));
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] CreateTeamDTO Team)
        {
            var validator = new TeamsValidator();
            Teams newTeam = mapper.Map<Teams>(Team);
            if (!validator.Validate(newTeam).IsValid)
                return BadRequest(validator.Validate(newTeam).Errors);
            db.Teams.Add(newTeam);
            await db.SaveChangesAsync();
            return Created("", mapper.Map<TeamDTO>(newTeam));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TeamDTO>> UpdateTeam(int id, [FromBody] UpdateTeamDTO Team)
        {
            var validator = new TeamsValidator();
            Teams newTeam = mapper.Map<Teams>(Team);
            if (!validator.Validate(newTeam).IsValid)
                return BadRequest(validator.Validate(newTeam).Errors);
            var updated = mapper.Map<Teams>(Team); 
            var exist = await db.Teams.FirstOrDefaultAsync(x => x.Id == id);
            if (exist != null)
            {
                updated.Id = exist.Id;
                db.Teams.Remove(exist);
                db.Teams.Add(updated);
                await db.SaveChangesAsync();
            } 
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TeamDTO>> DeleteTeam(int id)
        {
            var Team = await db.Teams.FirstOrDefaultAsync(x => x.Id == id);
            if (Team != null)
            {
                db.Teams.Remove(Team);
                await db.SaveChangesAsync();
            }
            return NoContent();
        }
    }
}
