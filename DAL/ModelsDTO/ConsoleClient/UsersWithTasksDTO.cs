﻿using DAL.Entities;
using System.Collections.Generic; 

namespace DAL.ModelsDTO.ConsoleClient
{
    public class UsersWithTasksDTO : Users
    {
        public List<Tasks> Tasks { get; set; }
    }
}
