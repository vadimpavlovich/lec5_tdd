﻿using DAL.Entities;
using System.Collections.Generic;

namespace DAL.ModelsDTO.ConsoleClient
{
    public class TeamsSortedDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Users> Users { get; set; }
    }
}
