﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: true),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: true),
                    PerformerId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2018, 7, 18, 3, 33, 37, 696, DateTimeKind.Local).AddTicks(7774), "Автомобильное & Одежда" },
                    { 18, new DateTime(2020, 8, 21, 17, 0, 2, 452, DateTimeKind.Local).AddTicks(2605), "музыка & Меха" },
                    { 17, new DateTime(2020, 12, 28, 13, 42, 54, 446, DateTimeKind.Local).AddTicks(3266), "Спорт & детское" },
                    { 16, new DateTime(2018, 9, 11, 19, 28, 22, 875, DateTimeKind.Local).AddTicks(1761), "Дом" },
                    { 15, new DateTime(2019, 2, 12, 0, 34, 30, 815, DateTimeKind.Local).AddTicks(6389), "садинструмент" },
                    { 14, new DateTime(2021, 1, 23, 13, 49, 43, 502, DateTimeKind.Local).AddTicks(3431), "Игрушки" },
                    { 13, new DateTime(2019, 11, 19, 2, 48, 57, 886, DateTimeKind.Local).AddTicks(6858), "для малышей, Меха & Галантерея" },
                    { 12, new DateTime(2020, 3, 20, 16, 57, 19, 639, DateTimeKind.Local).AddTicks(4874), "Дом & Спорт" },
                    { 11, new DateTime(2021, 4, 22, 10, 52, 56, 269, DateTimeKind.Local).AddTicks(4631), "Игрушки & игры" },
                    { 10, new DateTime(2018, 3, 31, 0, 4, 33, 226, DateTimeKind.Local).AddTicks(1880), "красота" },
                    { 9, new DateTime(2019, 12, 16, 9, 13, 33, 219, DateTimeKind.Local).AddTicks(9495), "Бакалея, Пряжа & музыка" },
                    { 8, new DateTime(2020, 7, 8, 2, 16, 31, 860, DateTimeKind.Local).AddTicks(1751), "компьютеры, игры & украшения" },
                    { 7, new DateTime(2020, 4, 22, 19, 48, 52, 714, DateTimeKind.Local).AddTicks(7481), "Фильмы, Фильмы & Спорт" },
                    { 6, new DateTime(2019, 12, 7, 6, 37, 12, 821, DateTimeKind.Local).AddTicks(4172), "компьютеры, здоровье & туризм" },
                    { 5, new DateTime(2017, 12, 7, 20, 47, 52, 477, DateTimeKind.Local).AddTicks(7271), "детское, Одежда & детское" },
                    { 4, new DateTime(2018, 8, 28, 21, 25, 35, 443, DateTimeKind.Local).AddTicks(3031), "красота" },
                    { 3, new DateTime(2019, 7, 6, 1, 4, 31, 511, DateTimeKind.Local).AddTicks(1082), "Одежда" },
                    { 2, new DateTime(2019, 9, 28, 2, 0, 54, 540, DateTimeKind.Local).AddTicks(5934), "Спорт & Книги" },
                    { 19, new DateTime(2019, 10, 31, 13, 54, 0, 343, DateTimeKind.Local).AddTicks(4643), "туризм" },
                    { 20, new DateTime(2017, 10, 16, 21, 27, 4, 894, DateTimeKind.Local).AddTicks(7870), "садинструмент, здоровье & садинструмент" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2012, 9, 2, 0, 0, 0, 0, DateTimeKind.Local), "Ekaterina_Shiryaeva36@ya.ru", "Екатерина", "Ширяева", new DateTime(2021, 4, 11, 15, 39, 7, 153, DateTimeKind.Local).AddTicks(5358), 1 },
                    { 10, new DateTime(1999, 5, 2, 0, 0, 0, 0, DateTimeKind.Local), "Taisiya_Konovalova@yandex.ru", "Таисия", "Коновалова", new DateTime(2010, 8, 24, 19, 31, 31, 136, DateTimeKind.Local).AddTicks(4210), 20 },
                    { 16, new DateTime(2012, 4, 11, 0, 0, 0, 0, DateTimeKind.Local), "Fyodor.Lobanov@yahoo.com", "Фёдор", "Лобанов", new DateTime(2021, 5, 5, 15, 52, 0, 158, DateTimeKind.Local).AddTicks(7491), 18 },
                    { 13, new DateTime(2000, 5, 25, 0, 0, 0, 0, DateTimeKind.Local), "Olga_Aveeva69@hotmail.com", "Ольга", "Авдеева", new DateTime(2009, 2, 6, 3, 23, 48, 686, DateTimeKind.Local).AddTicks(9412), 18 },
                    { 9, new DateTime(1997, 4, 8, 0, 0, 0, 0, DateTimeKind.Local), "Aleksandr_Nikolaev54@yandex.ru", "Александр", "Николаев", new DateTime(2016, 4, 25, 16, 54, 57, 400, DateTimeKind.Local).AddTicks(8709), 18 },
                    { 4, new DateTime(2011, 9, 17, 0, 0, 0, 0, DateTimeKind.Local), "Angelina36@ya.ru", "Ангелина", "Новикова", new DateTime(2020, 4, 18, 18, 12, 15, 265, DateTimeKind.Local).AddTicks(5748), 13 },
                    { 14, new DateTime(2012, 8, 19, 0, 0, 0, 0, DateTimeKind.Local), "Egor61@ya.ru", "Егор", "Дементьев", new DateTime(2021, 3, 31, 23, 39, 23, 242, DateTimeKind.Local).AddTicks(575), 12 },
                    { 8, new DateTime(1999, 8, 14, 0, 0, 0, 0, DateTimeKind.Local), "Vadim3@ya.ru", "Вадим", "Кошелев", new DateTime(2008, 10, 27, 2, 54, 11, 490, DateTimeKind.Local).AddTicks(168), 12 },
                    { 18, new DateTime(2014, 2, 19, 0, 0, 0, 0, DateTimeKind.Local), "Oleg87@yahoo.com", "Олег", "Алексеев", new DateTime(2021, 12, 4, 19, 4, 28, 241, DateTimeKind.Local).AddTicks(4662), 11 },
                    { 7, new DateTime(2001, 11, 25, 0, 0, 0, 0, DateTimeKind.Local), "Arkadii28@yandex.ru", "Аркадий", "Голубев", new DateTime(2016, 12, 24, 13, 34, 44, 800, DateTimeKind.Local).AddTicks(8855), 10 },
                    { 17, new DateTime(1996, 6, 9, 0, 0, 0, 0, DateTimeKind.Local), "Alyona.Eliseeva@yahoo.com", "Алёна", "Елисеева", new DateTime(2020, 8, 28, 11, 26, 7, 899, DateTimeKind.Local).AddTicks(7952), 9 },
                    { 6, new DateTime(1998, 9, 21, 0, 0, 0, 0, DateTimeKind.Local), "Semyon.Sitnikov@mail.ru", "Семён", "Ситников", new DateTime(2015, 5, 6, 1, 55, 36, 456, DateTimeKind.Local).AddTicks(1804), 9 },
                    { 5, new DateTime(1999, 10, 2, 0, 0, 0, 0, DateTimeKind.Local), "Lyudmila.Kozlova@yandex.ru", "Людмила", "Козлова", new DateTime(2020, 2, 22, 9, 33, 36, 579, DateTimeKind.Local).AddTicks(1544), 9 },
                    { 20, new DateTime(2008, 3, 14, 0, 0, 0, 0, DateTimeKind.Local), "Anastasiya.Gureva@mail.ru", "Анастасия", "Гурьева", new DateTime(2019, 4, 17, 4, 44, 44, 706, DateTimeKind.Local).AddTicks(2153), 8 },
                    { 19, new DateTime(2010, 12, 7, 0, 0, 0, 0, DateTimeKind.Local), "Stepan.Strelkov58@hotmail.com", "Степан", "Стрелков", new DateTime(2020, 7, 23, 9, 25, 30, 27, DateTimeKind.Local).AddTicks(4779), 8 },
                    { 3, new DateTime(2001, 11, 7, 0, 0, 0, 0, DateTimeKind.Local), "Antonida53@yahoo.com", "Антонида", "Белова", new DateTime(2014, 8, 29, 12, 8, 51, 964, DateTimeKind.Local).AddTicks(6834), 7 },
                    { 2, new DateTime(2001, 5, 21, 0, 0, 0, 0, DateTimeKind.Local), "Aleksandr_Kolesnikov@gmail.com", "Александр", "Колесников", new DateTime(2010, 12, 12, 1, 43, 48, 742, DateTimeKind.Local).AddTicks(1496), 4 },
                    { 15, new DateTime(1999, 6, 29, 0, 0, 0, 0, DateTimeKind.Local), "Sofiya.Soboleva@mail.ru", "София", "Соболева", new DateTime(2016, 7, 22, 23, 56, 26, 793, DateTimeKind.Local).AddTicks(3158), 3 },
                    { 11, new DateTime(2002, 10, 17, 0, 0, 0, 0, DateTimeKind.Local), "Nikolai97@mail.ru", "Николай", "Самсонов", new DateTime(2021, 5, 10, 6, 10, 1, 789, DateTimeKind.Local).AddTicks(4190), 20 },
                    { 12, new DateTime(2008, 5, 17, 0, 0, 0, 0, DateTimeKind.Local), "Lyudmila49@mail.ru", "Людмила", "Киселева", new DateTime(2021, 4, 10, 18, 16, 59, 876, DateTimeKind.Local).AddTicks(7078), 20 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 12, 1, new DateTime(2018, 6, 29, 14, 21, 23, 910, DateTimeKind.Local).AddTicks(6623), new DateTime(2018, 8, 17, 15, 46, 19, 819, DateTimeKind.Local).AddTicks(1898), "Обучения реализация нашей современного консультация порядка обеспечивает проблем проблем потребностям.\nКонсультация изменений прогресса задача.", "Ножницы", 1 },
                    { 5, 10, new DateTime(2018, 7, 5, 4, 15, 47, 636, DateTimeKind.Local).AddTicks(1582), new DateTime(2020, 10, 21, 0, 34, 46, 111, DateTimeKind.Local).AddTicks(7997), "Нас технологий деятельности существующий рост сложившаяся актуальность профессионального общества оценить.\nПуть богатый значение широкому модели шагов прогресса обеспечивает правительством.\nСоциально-экономическое воздействия для новая последовательного сложившаяся.", "Кепка", 13 },
                    { 13, 13, new DateTime(2018, 2, 23, 11, 47, 39, 630, DateTimeKind.Local).AddTicks(5842), new DateTime(2019, 3, 14, 1, 6, 47, 975, DateTimeKind.Local).AddTicks(307), "Формированию модели технологий.\nНасущным формированию концепция же специалистов информационно-пропогандистское в.", "Автомобиль", 13 },
                    { 9, 18, new DateTime(2018, 9, 11, 1, 29, 5, 289, DateTimeKind.Local).AddTicks(9102), new DateTime(2021, 5, 8, 14, 50, 3, 669, DateTimeKind.Local).AddTicks(5764), "Высокотехнологичная занимаемых повышению опыт базы проверки.\nПоставленных предложений проект правительством.\nУкрепления укрепления сознания.\nВызывает сущности активом степени.", "Куртка", 8 },
                    { 3, 7, new DateTime(2019, 4, 23, 17, 6, 30, 978, DateTimeKind.Local).AddTicks(5628), new DateTime(2020, 4, 18, 15, 19, 4, 236, DateTimeKind.Local).AddTicks(7264), "Уровня влечёт кадровой современного начало понимание на а отметить.", "Стол", 18 },
                    { 7, 17, new DateTime(2018, 3, 9, 1, 12, 14, 108, DateTimeKind.Local).AddTicks(762), new DateTime(2021, 3, 24, 0, 24, 0, 258, DateTimeKind.Local).AddTicks(607), "Подготовке структуры отметить укрепления количественный.\nУправление участниками сфера высшего постоянный курс прежде консультация.", "Майка", 4 },
                    { 17, 20, new DateTime(2020, 7, 9, 17, 22, 59, 929, DateTimeKind.Local).AddTicks(8451), new DateTime(2020, 11, 25, 2, 28, 37, 656, DateTimeKind.Local).AddTicks(7507), "Концепция очевидна рост новых.", "Стул", 15 },
                    { 1, 20, new DateTime(2018, 9, 10, 12, 59, 52, 576, DateTimeKind.Local).AddTicks(2776), new DateTime(2020, 1, 23, 3, 41, 19, 257, DateTimeKind.Local).AddTicks(5174), "Условий новая обеспечение поставленных направлений.\nЗначимость забывать следует показывает модели высокотехнологичная таким новых.\nСистему ресурсосберегающих участия способствует предложений концепция занимаемых участия качества.", "Берет", 17 },
                    { 20, 19, new DateTime(2017, 12, 29, 19, 1, 8, 75, DateTimeKind.Local).AddTicks(7190), new DateTime(2020, 11, 6, 14, 6, 25, 758, DateTimeKind.Local).AddTicks(6685), "Настолько формирования экономической системы обществом анализа соответствующей.\nСтепени ресурсосберегающих обеспечивает организационной образом работы повседневная.", "Портмоне", 16 },
                    { 16, 19, new DateTime(2018, 8, 30, 11, 6, 39, 778, DateTimeKind.Local).AddTicks(7823), new DateTime(2021, 2, 27, 2, 17, 3, 812, DateTimeKind.Local).AddTicks(433), "Степени потребностям настолько дальнейших забывать.", "Кошелек", 3 },
                    { 8, 19, new DateTime(2017, 10, 15, 19, 5, 20, 831, DateTimeKind.Local).AddTicks(9408), new DateTime(2017, 12, 6, 23, 46, 2, 260, DateTimeKind.Local).AddTicks(3059), "Дальнейших участия деятельности позволяет определения от укрепления отметить.", "Плащ", 8 },
                    { 19, 3, new DateTime(2021, 5, 5, 5, 1, 58, 546, DateTimeKind.Local).AddTicks(1033), new DateTime(2021, 12, 19, 6, 14, 57, 776, DateTimeKind.Local).AddTicks(8527), "Собой соответствующей нами организационной.\nСтороны участниками кругу.\nСфера не сознания структура.\nВыполнять настолько в.", "Майка", 16 },
                    { 14, 3, new DateTime(2021, 5, 11, 10, 48, 52, 954, DateTimeKind.Local).AddTicks(4948), new DateTime(2022, 1, 17, 3, 25, 38, 337, DateTimeKind.Local).AddTicks(3272), "С важные интересный рост.", "Ремень", 18 },
                    { 4, 3, new DateTime(2021, 1, 26, 18, 54, 30, 692, DateTimeKind.Local).AddTicks(2519), new DateTime(2021, 6, 18, 15, 45, 7, 663, DateTimeKind.Local).AddTicks(2026), "Инновационный важные настолько кадровой насущным степени инновационный.", "Сабо", 13 },
                    { 10, 15, new DateTime(2019, 2, 3, 18, 35, 25, 794, DateTimeKind.Local).AddTicks(8302), new DateTime(2019, 8, 23, 10, 48, 27, 324, DateTimeKind.Local).AddTicks(8402), "Этих структура влечёт формировании стороны однако этих социально-экономическое высокотехнологичная.\nВажную современного опыт.", "Стул", 14 },
                    { 2, 15, new DateTime(2018, 3, 2, 22, 28, 6, 130, DateTimeKind.Local).AddTicks(2635), new DateTime(2018, 5, 18, 13, 20, 19, 602, DateTimeKind.Local).AddTicks(4737), "Форм реализация широким мира модернизации.\nОбеспечивает социально-ориентированный экономической с высокотехнологичная анализа соображения.\nНами качественно равным различных повседневная правительством от.", "Стул", 9 },
                    { 18, 1, new DateTime(2020, 6, 23, 11, 0, 1, 978, DateTimeKind.Local).AddTicks(6357), new DateTime(2021, 4, 22, 19, 32, 2, 226, DateTimeKind.Local).AddTicks(8041), "Обеспечение повышение собой общества рост собой степени широкому обеспечение однако.\nТаким шагов постоянный реализация процесс.", "Кулон", 13 },
                    { 15, 1, new DateTime(2019, 10, 31, 1, 1, 56, 231, DateTimeKind.Local).AddTicks(1241), new DateTime(2020, 4, 9, 18, 13, 27, 767, DateTimeKind.Local).AddTicks(1454), "По же новая рост задача.", "Кошелек", 17 },
                    { 6, 11, new DateTime(2020, 10, 30, 8, 41, 42, 903, DateTimeKind.Local).AddTicks(9985), new DateTime(2022, 2, 18, 6, 7, 55, 337, DateTimeKind.Local).AddTicks(8065), "Шагов порядка собой существующий финансовых образом процесс интересный структуры.\nПрогресса нас информационно-пропогандистское форм национальный изменений в кадров.\nОбщественной количественный разработке.", "Кепка", 7 },
                    { 11, 11, new DateTime(2019, 12, 12, 19, 34, 59, 293, DateTimeKind.Local).AddTicks(49), new DateTime(2021, 8, 16, 18, 41, 27, 13, DateTimeKind.Local).AddTicks(1887), "Поэтапного по специалистов сущности а инновационный.\nУчастия позиции вызывает.\nИдейные позиции качественно прежде массового.\nПостоянный широким кадровой однако также напрямую экономической.", "Стол", 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 2, new DateTime(2021, 5, 26, 22, 44, 17, 46, DateTimeKind.Local).AddTicks(1584), "Задача формировании уровня.\nСпециалистов оценить собой.\nЗначительной эксперимент за.", null, "Сознания шагов рост забывать общества не.", 11, 12, 0 },
                    { 20, new DateTime(2019, 2, 12, 6, 42, 29, 398, DateTimeKind.Local).AddTicks(9772), "Различных участия профессионального базы обеспечивает напрямую принимаемых прежде.\nНасущным занимаемых современного порядка таким работы забывать разработке.\nПоэтапного позволяет оценить повышению сфера высокотехнологичная.\nРазработке качественно проблем другой за национальный.", null, "Обществом рамки мира материально-технической.", 16, 6, 3 },
                    { 5, new DateTime(2018, 12, 12, 0, 42, 8, 681, DateTimeKind.Local).AddTicks(4533), "Постоянное кадровой формированию общественной последовательного задания демократической деятельности задача повышение.\nКадровой другой участниками мира условий таким повседневной нашей разнообразный по.\nПравительством практика особенности структура для практика таким кадров опыт нами.\nСтороны материально-технической постоянное активом качественно кадров другой принимаемых.", new DateTime(2019, 9, 22, 5, 0, 3, 365, DateTimeKind.Local).AddTicks(7604), "Создание поэтапного от различных.", 12, 6, 2 },
                    { 1, new DateTime(2019, 11, 18, 4, 52, 24, 287, DateTimeKind.Local).AddTicks(1571), "Оценить дальнейших форм широкому важные прогресса подготовке различных оценить обуславливает.", new DateTime(2020, 3, 23, 12, 7, 14, 965, DateTimeKind.Local).AddTicks(7120), "Соответствующих предложений для.", 2, 5, 3 },
                    { 7, new DateTime(2019, 5, 12, 22, 55, 4, 205, DateTimeKind.Local).AddTicks(2516), "Принимаемых активом соответствующих воздействия форм систему высшего.\nКачественно соответствующих сложившаяся систему консультация повседневной уточнения базы соответствующих.", null, "Важную место идейные разнообразный формировании путь.", 1, 9, 2 },
                    { 14, new DateTime(2021, 2, 7, 9, 57, 13, 29, DateTimeKind.Local).AddTicks(7062), "Национальный прогресса мира.\nСпособствует высокотехнологичная место задания забывать путь внедрения кругу.\nУровня кадровой позиции задач идейные организации обуславливает.", new DateTime(2021, 3, 20, 19, 9, 35, 66, DateTimeKind.Local).AddTicks(5391), "Работы повышение национальный модель.", 5, 20, 3 },
                    { 12, new DateTime(2019, 3, 3, 14, 23, 38, 114, DateTimeKind.Local).AddTicks(3894), "Модели уровня кругу форм форм.\nНастолько демократической существующий современного организационной.\nПрактика обеспечение влечёт обучения влечёт для же.", new DateTime(2020, 1, 19, 4, 1, 46, 769, DateTimeKind.Local).AddTicks(6371), "Выбранный опыт прогресса значение.", 10, 8, 2 },
                    { 15, new DateTime(2018, 5, 22, 8, 30, 3, 550, DateTimeKind.Local).AddTicks(4659), "Играет консультация разработке.\nКачества модель изменений и предложений кадров мира формированию.\nОбеспечение забывать структура сфера.\nЭксперимент модернизации практика целесообразности современного реализация деятельности административных зависит качественно.", new DateTime(2019, 12, 2, 1, 51, 33, 219, DateTimeKind.Local).AddTicks(1484), "Способствует уточнения формированию социально-ориентированный.", 7, 19, 0 },
                    { 10, new DateTime(2018, 3, 17, 9, 58, 23, 469, DateTimeKind.Local).AddTicks(6638), "Массового укрепления анализа мира.", null, "Сомнений для модель различных.", 19, 14, 0 },
                    { 13, new DateTime(2019, 5, 10, 23, 57, 13, 5, DateTimeKind.Local).AddTicks(5795), "Сомнений финансовых шагов не требует позиции предложений ресурсосберегающих социально-ориентированный мира.\nСледует а профессионального.\nСпециалистов прогресса активности интересный.", null, "Для процесс участия понимание обществом.", 18, 4, 1 },
                    { 6, new DateTime(2018, 1, 19, 22, 19, 27, 252, DateTimeKind.Local).AddTicks(660), "Актуальность консультация другой нашей финансовых современного соображения модель также активизации.\nНе интересный идейные задания важную широким обуславливает в массового.", new DateTime(2021, 2, 8, 16, 21, 51, 702, DateTimeKind.Local).AddTicks(6661), "На нас обеспечивает важные показывает соображения.", 18, 4, 1 },
                    { 8, new DateTime(2019, 1, 16, 23, 27, 22, 759, DateTimeKind.Local).AddTicks(7652), "Внедрения прежде очевидна.\nПравительством социально-экономическое формированию модернизации зависит модели.\nНе ресурсосберегающих вызывает социально-ориентированный качества значение плановых образом.\nВсего занимаемых специалистов социально-экономическое различных.", null, "Потребностям в разнообразный организации общества.", 1, 10, 2 },
                    { 4, new DateTime(2019, 4, 11, 23, 32, 43, 114, DateTimeKind.Local).AddTicks(2555), "Насущным предложений структуры новая административных принципов качественно подготовке сомнений.\nНамеченных широким нас.\nВлечёт настолько нашей что социально-ориентированный.\nАктуальность позволяет нашей концепция значительной качества высокотехнологичная роль постоянное реализация.", new DateTime(2020, 8, 7, 15, 32, 18, 877, DateTimeKind.Local).AddTicks(9834), "Административных на значимость курс богатый.", 5, 2, 2 },
                    { 19, new DateTime(2020, 6, 12, 6, 10, 39, 964, DateTimeKind.Local).AddTicks(5365), "Таким понимание следует соответствующих количественный уточнения на развития на задач.\nФормировании уточнения всего повседневной уровня.", null, "Концепция таким структуры всего насущным.", 12, 18, 3 },
                    { 18, new DateTime(2021, 3, 26, 21, 13, 2, 142, DateTimeKind.Local).AddTicks(8052), "Для степени важные равным опыт актуальность.\nПовышению особенности порядка особенности организационной соображения начало идейные.", null, "Процесс сфера разнообразный же начало.", 3, 18, 1 },
                    { 16, new DateTime(2018, 9, 24, 18, 27, 38, 204, DateTimeKind.Local).AddTicks(170), "Особенности особенности обеспечение проблем постоянный принимаемых структуры повседневной начало эксперимент.\nТехнологий значительной предпосылки обеспечение общества кадров формировании финансовых забывать.\nСистемы влечёт предложений плановых позиции организационной формировании кругу создаёт.\nРост роль настолько кадровой другой обучения.", new DateTime(2020, 5, 2, 8, 45, 16, 908, DateTimeKind.Local).AddTicks(8299), "Систему следует прежде.", 16, 18, 0 },
                    { 11, new DateTime(2019, 7, 5, 21, 32, 10, 170, DateTimeKind.Local).AddTicks(1288), "Порядка принципов целесообразности кадровой.\nДальнейших концепция таким насущным в.", new DateTime(2020, 8, 22, 17, 7, 32, 471, DateTimeKind.Local).AddTicks(6413), "Значительной значение социально-ориентированный актуальность.", 6, 15, 0 },
                    { 9, new DateTime(2021, 5, 6, 12, 55, 25, 94, DateTimeKind.Local).AddTicks(7298), "Последовательного соображения позиции соображения задач постоянное гражданского специалистов нашей.", null, "Насущным проект финансовых.", 18, 12, 1 },
                    { 3, new DateTime(2018, 7, 5, 18, 51, 14, 929, DateTimeKind.Local).AddTicks(8217), "В рост принципов.", null, "Активом путь инновационный следует потребностям процесс.", 20, 11, 2 },
                    { 17, new DateTime(2018, 5, 29, 14, 15, 37, 977, DateTimeKind.Local).AddTicks(1025), "Изменений развития социально-экономическое однако а разнообразный равным настолько курс количественный.", null, "Модернизации напрямую поэтапного.", 5, 11, 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
